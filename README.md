# TASK MANAGER

## DEVELOPER INFO

**NAME**: Nikolay Didyk

**E-MAIL**: no-such-email@fake.com

**E-MAIL**: another-fake@email.com

## SOFTWARE

**OS**: MS Windows 10

**JDK**: OpenJDK 1.8.0_322

## HARDWARE

**CPU**: AMD Ryzen 5 2600x

**RAM**: 16GB

**SSD**: 1Tb

## BUILD

```bash
mvn clean install
```


## RUN

```bash
java -jar ./task-manager.jar
```

