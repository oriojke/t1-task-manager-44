package ru.t1.didyk.taskmanager.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.didyk.taskmanager.api.endpoint.*;
import ru.t1.didyk.taskmanager.api.repository.ICommandRepository;
import ru.t1.didyk.taskmanager.api.service.*;
import ru.t1.didyk.taskmanager.command.AbstractCommand;
import ru.t1.didyk.taskmanager.exception.system.ArgumentNotSupportedException;
import ru.t1.didyk.taskmanager.exception.system.CommandNotSupportedException;
import ru.t1.didyk.taskmanager.repository.CommandRepository;
import ru.t1.didyk.taskmanager.service.CommandService;
import ru.t1.didyk.taskmanager.service.LoggerService;
import ru.t1.didyk.taskmanager.service.PropertyService;
import ru.t1.didyk.taskmanager.service.TokenService;
import ru.t1.didyk.taskmanager.util.SystemUtil;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.didyk.taskmanager.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final ITokenService tokenService = new TokenService();

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpointClient = IDomainEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpointClient = IProjectEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpointClient = ISystemEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpointClient = ITaskEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            try {
                registry(clazz);
            } catch (@NotNull final Exception e) {
                throw new RuntimeException(e);
            }

        }
    }


    public void run(@Nullable String[] args) {
        processArguments(args);

        //initBackup();
        initFileScanner();
        initLogger();
        initPID();

        processCommands();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable String[] args) {
        if (args == null || args.length == 0) return;
        @NotNull final String argument = args[0];
        processArgument(argument);
    }

    public void processCommand(@NotNull String command, final boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void processCommand(@NotNull String command) {
        processCommand(command, true);
    }

    private void processArgument(@NotNull String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("** WELCOME THE TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        backup.stop();
        fileScanner.stop();
    }

    public void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) throws InstantiationException, IllegalAccessException {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initBackup() {
        backup.init();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

}
