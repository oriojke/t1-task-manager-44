package ru.t1.didyk.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.request.UserLogoutRequest;
import ru.t1.didyk.taskmanager.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "logout";
    @NotNull
    public static final String DESCRIPTION = "Log out current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        request.setToken(getToken());
        serviceLocator.getAuthEndpointClient().logout(request);
        setToken(null);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }
}
