package ru.t1.didyk.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.request.UserUpdateProfileRequest;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "update-user-profile";
    @NotNull
    public static final String DESCRIPTION = "Update user profile.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER PROFILE UPDATE]");
        System.out.println("FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        serviceLocator.getUserEndpointClient().updateProfile(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }
}
