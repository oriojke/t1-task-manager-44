package ru.t1.didyk.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.request.ProjectListRequest;
import ru.t1.didyk.taskmanager.dto.response.ProjectListResponse;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.dto.model.ProjectDTO;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-list";
    @NotNull
    public static final String DESCRIPTION = "Display all projects.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSortType(sort);
        int index = 1;
        @Nullable final ProjectListResponse response = serviceLocator.getProjectEndpointClient().listProjects(request);
        if (response.getProjects() == null) response.setProjects(Collections.emptyList());
        @Nullable final List<ProjectDTO> projects = response.getProjects();
        for (@Nullable final ProjectDTO project : projects) {
            if (project == null) continue;
            System.out.println(index + "." + project);
            index++;
        }
    }
}
