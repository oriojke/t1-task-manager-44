package ru.t1.didyk.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.ProjectCreateRequest;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-create";
    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        serviceLocator.getProjectEndpointClient().create(request);
    }
}
