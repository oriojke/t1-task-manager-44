package ru.t1.didyk.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-clear";
    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        serviceLocator.getProjectEndpointClient().clearProject(request);
    }
}
