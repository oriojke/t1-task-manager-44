package ru.t1.didyk.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.model.ISessionRepository;
import ru.t1.didyk.taskmanager.model.Session;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable List<Session> findAll(@Nullable String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final String sql = "SELECT m FROM Session m WHERE m.user.id = :userId";
        return entityManager.createQuery(sql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable Session findOneById(@Nullable String userId, @Nullable String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String sql = "SELECT m FROM Session m WHERE m.user.id = :userId and m.id = :id";
        return entityManager.createQuery(sql, Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public @NotNull List<Session> findAll() {
        @NotNull final String sql = "SELECT m FROM Session m WHERE m.user.id = :userId";
        return entityManager.createQuery(sql, Session.class)
                .getResultList();
    }

    @Override
    public @Nullable Session findOneById(@NotNull String id) {
        @NotNull final String sql = "SELECT m FROM Session m WHERE m.user.id = :userId and m.id = :id";
        return entityManager.createQuery(sql, Session.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }
}
