package ru.t1.didyk.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.model.IUserRepository;
import ru.t1.didyk.taskmanager.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable User findById(@NotNull String id) {
        return findOneById(id);
    }

    @Override
    public @Nullable User findByLogin(@NotNull String login) {
        @NotNull final String sql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(sql, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable User findByEmail(@NotNull String email) {
        @NotNull final String sql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(sql, User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Boolean isLoginExists(@NotNull String login) {
        return findByLogin(login) != null;
    }

    @Override
    public @Nullable Boolean isEmailExists(@NotNull String email) {
        return findByEmail(email) != null;
    }

    @Override
    public @Nullable User findOneById(@NotNull String id) {
        @NotNull final String sql = "SELECT m FROM User m WHERE m.id = :id";
        return entityManager.createQuery(sql, User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public @NotNull List<User> findAll() {
        @NotNull final String sql = "SELECT m FROM User m";
        return entityManager.createQuery(sql, User.class)
                .getResultList();
    }
}
