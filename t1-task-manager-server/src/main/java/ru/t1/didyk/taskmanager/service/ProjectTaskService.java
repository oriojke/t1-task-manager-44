package ru.t1.didyk.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.service.IProjectTaskService;
import ru.t1.didyk.taskmanager.api.service.dto.IProjectDtoService;
import ru.t1.didyk.taskmanager.api.service.dto.ITaskDtoService;
import ru.t1.didyk.taskmanager.dto.model.TaskDTO;
import ru.t1.didyk.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.didyk.taskmanager.exception.field.TaskIdEmptyException;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectDtoService projectService;

    @NotNull
    private final ITaskDtoService taskService;

    public ProjectTaskService(@NotNull IProjectDtoService projectService, @NotNull ITaskDtoService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) return;
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        for (@NotNull final TaskDTO task : tasks) taskService.removeById(userId, task.getId());
        projectService.removeById(userId, projectId);
    }
}