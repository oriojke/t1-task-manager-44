package ru.t1.didyk.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.model.ITaskRepository;
import ru.t1.didyk.taskmanager.dto.model.TaskDTO;
import ru.t1.didyk.taskmanager.model.Task;
import ru.t1.didyk.taskmanager.model.User;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @NotNull Task create(@NotNull String userId, @NotNull String name, @NotNull String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(entityManager.find(User.class, userId));
        return add(userId, task);
    }

    @Override
    public @NotNull Task create(@NotNull String userId, @NotNull String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUser(entityManager.find(User.class, userId));
        return add(userId, task);

    }

    @Override
    public @NotNull List<Task> findAllByProjectId(@NotNull String userId, @Nullable String projectId) {
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        @NotNull final String sql = "SELECT m FROM Task m WHERE m.user.id = :userId and m.projectId = :projectId";
        return entityManager.createQuery(sql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final String sql = "SELECT m FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(sql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable Task findOneById(@Nullable String userId, @Nullable String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String sql = "SELECT m FROM Task m WHERE m.user.id = :userId and m.id = :id";
        return entityManager.createQuery(sql, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public @NotNull List<Task> findAll() {
        @NotNull final String sql = "SELECT m FROM Task m WHERE m.user.id = :userId";
        return entityManager.createQuery(sql, Task.class)
                .getResultList();
    }

    @Override
    public @Nullable Task findOneById(@NotNull String id) {
        @NotNull final String sql = "SELECT m FROM Task m WHERE m.user.id = :userId and m.id = :id";
        return entityManager.createQuery(sql, Task.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }
}
