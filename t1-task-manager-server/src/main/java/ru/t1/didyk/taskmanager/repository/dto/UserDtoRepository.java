package ru.t1.didyk.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.dto.IUserDtoRepository;
import ru.t1.didyk.taskmanager.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable UserDTO findById(@NotNull String id) {
        return findOneById(id);
    }

    @Override
    public @Nullable UserDTO findByLogin(@NotNull String login) {
        @NotNull final String sql = "SELECT m FROM UserDTO m WHERE m.login = :login";
        return entityManager.createQuery(sql, UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable UserDTO findByEmail(@NotNull String email) {
        @NotNull final String sql = "SELECT m FROM UserDTO m WHERE m.email = :email";
        return entityManager.createQuery(sql, UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Boolean isLoginExists(@NotNull String login) {
        return findByLogin(login) != null;
    }

    @Override
    public @Nullable Boolean isEmailExists(@NotNull String email) {
        return findByEmail(email) != null;
    }

    @Override
    public @Nullable UserDTO findOneById(@NotNull String id) {
        @NotNull final String sql = "SELECT m FROM UserDTO m WHERE m.id = :id";
        return entityManager.createQuery(sql, UserDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public @NotNull List<UserDTO> findAll() {
        @NotNull final String sql = "SELECT m FROM UserDTO m";
        return entityManager.createQuery(sql, UserDTO.class)
                .getResultList();
    }
}
