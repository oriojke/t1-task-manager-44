package ru.t1.didyk.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.dto.ITaskDtoRepository;
import ru.t1.didyk.taskmanager.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> implements ITaskDtoRepository {

    public TaskDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @NotNull TaskDTO create(@NotNull String userId, @NotNull String name, @NotNull String description) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(userId, task);
    }

    @Override
    public @NotNull TaskDTO create(@NotNull String userId, @NotNull String name) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setUserId(userId);
        return add(userId, task);
    }

    @Override
    public @NotNull List<TaskDTO> findAllByProjectId(@NotNull String userId, @Nullable String projectId) {
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        @NotNull final String sql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId and m.projectId = :projectId";
        return entityManager.createQuery(sql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@Nullable String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final String sql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(sql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable TaskDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String sql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId and m.id = :id";
        return entityManager.createQuery(sql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public @NotNull List<TaskDTO> findAll() {
        @NotNull final String sql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(sql, TaskDTO.class)
                .getResultList();
    }

    @Override
    public @Nullable TaskDTO findOneById(@NotNull String id) {
        @NotNull final String sql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId and m.id = :id";
        return entityManager.createQuery(sql, TaskDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }
}
