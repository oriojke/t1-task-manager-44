package ru.t1.didyk.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.dto.IProjectDtoRepository;
import ru.t1.didyk.taskmanager.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @NotNull ProjectDTO create(@NotNull String userId, @NotNull String name, @NotNull String description) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(userId, project);
    }

    @Override
    public @NotNull ProjectDTO create(@NotNull String userId, @NotNull String name) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        return add(userId, project);
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@Nullable String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final String sql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(sql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable ProjectDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String sql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId and m.id = :id";
        return entityManager.createQuery(sql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public @NotNull List<ProjectDTO> findAll() {
        @NotNull final String sql = "SELECT m FROM ProjectDTO m";
        return entityManager.createQuery(sql, ProjectDTO.class)
                .getResultList();
    }

    @Override
    public @Nullable ProjectDTO findOneById(@NotNull String id) {
        @NotNull final String sql = "SELECT m FROM ProjectDTO m WHERE m.id = :id";
        return entityManager.createQuery(sql, ProjectDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }
}
