package ru.t1.didyk.taskmanager.api.service.model;

import ru.t1.didyk.taskmanager.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
