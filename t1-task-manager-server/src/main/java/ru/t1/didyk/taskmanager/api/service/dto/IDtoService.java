package ru.t1.didyk.taskmanager.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.dto.IDtoRepository;
import ru.t1.didyk.taskmanager.dto.model.AbstractModelDTO;
import ru.t1.didyk.taskmanager.enumerated.Sort;

import java.util.List;

public interface IDtoService<M extends AbstractModelDTO> extends IDtoRepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort);

}
