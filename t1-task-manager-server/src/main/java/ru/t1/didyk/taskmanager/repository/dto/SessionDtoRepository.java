package ru.t1.didyk.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.dto.ISessionDtoRepository;
import ru.t1.didyk.taskmanager.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable List<SessionDTO> findAll(@Nullable String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final String sql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(sql, SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable SessionDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String sql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId and m.id = :id";
        return entityManager.createQuery(sql, SessionDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public @NotNull List<SessionDTO> findAll() {
        @NotNull final String sql = "SELECT m FROM SessionDTO m";
        return entityManager.createQuery(sql, SessionDTO.class)
                .getResultList();
    }

    @Override
    public @Nullable SessionDTO findOneById(@NotNull String id) {
        @NotNull final String sql = "SELECT m FROM SessionDTO m WHERE m.id = :id";
        return entityManager.createQuery(sql, SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }
}
