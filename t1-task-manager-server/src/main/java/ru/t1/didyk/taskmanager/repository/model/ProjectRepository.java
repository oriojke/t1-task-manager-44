package ru.t1.didyk.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.model.IProjectRepository;
import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.model.User;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @NotNull Project create(@NotNull String userId, @NotNull String name, @NotNull String description) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(entityManager.find(User.class, userId));
        return add(userId, project);
    }

    @Override
    public @NotNull Project create(@NotNull String userId, @NotNull String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUser(entityManager.find(User.class, userId));
        return add(userId, project);
    }

    @Override
    public @Nullable List<Project> findAll(@Nullable String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final String sql = "SELECT m FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(sql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable Project findOneById(@Nullable String userId, @Nullable String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String sql = "SELECT m FROM Project m WHERE m.user.id = :userId and m.id = :id";
        return entityManager.createQuery(sql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public @NotNull List<Project> findAll() {
        @NotNull final String sql = "SELECT m FROM Project m";
        return entityManager.createQuery(sql, Project.class)
                .getResultList();
    }

    @Override
    public @Nullable Project findOneById(@NotNull String id) {
        @NotNull final String sql = "SELECT m FROM Project m WHERE m.id = :id";
        return entityManager.createQuery(sql, Project.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }
}
