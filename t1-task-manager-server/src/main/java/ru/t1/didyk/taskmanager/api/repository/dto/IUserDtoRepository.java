package ru.t1.didyk.taskmanager.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.UserDTO;

public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    UserDTO findById(@NotNull String id);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @Nullable
    Boolean isLoginExists(@NotNull String login);

    @Nullable
    Boolean isEmailExists(@NotNull String email);

}
