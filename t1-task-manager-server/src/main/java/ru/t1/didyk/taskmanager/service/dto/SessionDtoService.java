package ru.t1.didyk.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.repository.dto.ISessionDtoRepository;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.api.service.dto.ISessionDtoService;
import ru.t1.didyk.taskmanager.dto.model.SessionDTO;
import ru.t1.didyk.taskmanager.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;

public class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository> implements ISessionDtoService {

    public SessionDtoService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull ISessionDtoRepository getRepository(@NotNull EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }
}
