package ru.t1.didyk.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.model.IUserOwnedRepository;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.api.service.model.IUserOwnedService;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.didyk.taskmanager.exception.field.IdEmptyException;
import ru.t1.didyk.taskmanager.exception.field.IndexIncorrectException;
import ru.t1.didyk.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.didyk.taskmanager.model.AbstractUserOwnedModel;
import ru.t1.didyk.taskmanager.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final EntityManager entityManager);

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            return repository.findAll(userId);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            if (comparator == null) return findAll(userId);
            return repository.findAll(userId, comparator);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            return repository.findAll();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            if (model == null) throw new ProjectNotFoundException();
            entityManager.getTransaction().begin();
            @Nullable final M object = repository.add(userId, model);
            entityManager.getTransaction().commit();
            return object;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public M add(@NotNull M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (model == null) throw new ProjectNotFoundException();
            entityManager.getTransaction().begin();
            @Nullable final M object = repository.add(model);
            entityManager.getTransaction().commit();
            return object;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (models.isEmpty()) return Collections.emptyList();
            entityManager.getTransaction().begin();
            @NotNull final Collection<M> objects = repository.add(models);
            entityManager.getTransaction().commit();
            return objects;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (models.isEmpty()) return Collections.emptyList();
            entityManager.getTransaction().begin();
            @NotNull final Collection<M> objects = repository.set(models);
            entityManager.getTransaction().commit();
            return objects;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            if (id == null || id.isEmpty()) return false;
            return repository.existsById(userId, id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable String id) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (id == null || id.isEmpty()) return false;
            return repository.existsById(id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            if (id == null || id.isEmpty()) throw new IdEmptyException();
            return repository.findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            if (index == null || index < 0) throw new IndexIncorrectException();
            return repository.findOneByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            return repository.getSize(userId);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            entityManager.getTransaction().begin();
            @Nullable final M object = repository.remove(userId, model);
            entityManager.getTransaction().commit();
            return object;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public M remove(@Nullable M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @Nullable final M object = repository.remove(model);
            entityManager.getTransaction().commit();
            return object;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            if (id == null || id.isEmpty()) throw new IdEmptyException();
            @Nullable final M model = repository.findOneById(userId, id);
            entityManager.getTransaction().begin();
            remove(userId, model);
            @Nullable final M object = model;
            entityManager.getTransaction().commit();
            return object;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            if (index == null || index < 0) throw new IndexIncorrectException();
            @Nullable final M model = repository.findOneByIndex(userId, index);
            entityManager.getTransaction().begin();
            repository.remove(userId, model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Override
    public void removeAll(@Nullable String userId) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable String userId, @NotNull M object) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        object.setUser(getEntityManager().find(User.class, userId));
        update(object);
    }
}
