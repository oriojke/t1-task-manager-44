package ru.t1.didyk.taskmanager.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.didyk.taskmanager.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.didyk.taskmanager.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedDtoRepository<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    List<M> findAll();

}
