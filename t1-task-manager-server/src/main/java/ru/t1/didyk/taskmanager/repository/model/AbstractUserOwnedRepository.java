package ru.t1.didyk.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.model.IUserOwnedRepository;
import ru.t1.didyk.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.didyk.taskmanager.model.AbstractUserOwnedModel;
import ru.t1.didyk.taskmanager.model.User;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable M add(@Nullable String userId, @NotNull M model) {
        if (userId.isEmpty()) return null;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.persist(model);
        return model;
    }

    @Override
    public @Nullable M remove(@Nullable String userId, @Nullable M model) {
        if (userId.isEmpty()) return null;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.remove(model);
        return model;
    }

    @Override
    public void update(@Nullable String userId, @NotNull M object) {
        if (userId.isEmpty()) return;
        object.setUser(entityManager.find(User.class, userId));
        entityManager.merge(object);
    }

    @Override
    public void clear(@Nullable String userId) {
        findAll(userId).forEach(m -> remove(m));
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public @Nullable M findOneByIndex(@Nullable String userId, @Nullable Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public @Nullable M removeById(@Nullable String userId, @Nullable String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public @Nullable M removeByIndex(@Nullable String userId, @Nullable Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public @Nullable List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public int getSize(@Nullable String userId) {
        return findAll(userId).size();
    }

    @Override
    public void removeAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        findAll(userId).forEach(model -> remove(model));
    }
}
