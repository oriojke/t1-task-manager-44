package ru.t1.didyk.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.marker.UnitCategory;
import ru.t1.didyk.taskmanager.service.dto.TaskDtoService;

import static ru.t1.didyk.taskmanager.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final TaskDtoService taskService = new TaskDtoService(connectionService);

    @Before
    public void before() {
        taskService.add(USER1.getId(), USER_TASK);
        taskService.add(USER2.getId(), ADMIN_TASK);
    }

    @After
    public void after() {
        taskService.clear(USER1.getId());
        taskService.clear(USER2.getId());
    }

    @Test
    public void add() {
        Assert.assertNotNull(taskService.add(USER1.getId(), USER_TASK2));
        Assert.assertNotNull(taskService.add(USER_TASK3));
        Assert.assertFalse(taskService.add(USER_TASK_LIST).isEmpty());
    }

    @Test
    public void changeTaskStatus() {
        Assert.assertNotNull(taskService.changeTaskStatusById(USER1.getId(), USER_TASK.getId(), Status.NOT_STARTED));
        Assert.assertNotNull(taskService.changeTaskStatusByIndex(USER1.getId(), 0, Status.IN_PROGRESS));
    }

    @Test
    public void create() {
        Assert.assertNotNull(taskService.create(USER1.getId(), "NEW NAME"));
        Assert.assertNotNull(taskService.create(USER1.getId(), "NEW NAME 2", "NEW DESCRIPTION"));
    }

    @Test
    public void clear() {
        taskService.clear(USER1.getId());
        Assert.assertTrue(taskService.findAll(USER1.getId()).isEmpty());
    }

    @Test
    public void find() {
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertFalse(taskService.findAll(USER1.getId()).isEmpty());
        Assert.assertFalse(taskService.findAll(USER1.getId(), Sort.BY_NAME.getComparator()).isEmpty());
        Assert.assertFalse(taskService.findAll(USER1.getId(), Sort.BY_STATUS.getComparator()).isEmpty());
        Assert.assertFalse(taskService.findAll(USER1.getId(), Sort.BY_CREATED.getComparator()).isEmpty());
        Assert.assertFalse(taskService.findAll(USER1.getId(), Sort.BY_NAME).isEmpty());
        Assert.assertFalse(taskService.findAll(USER1.getId(), Sort.BY_STATUS).isEmpty());
        Assert.assertFalse(taskService.findAll(USER1.getId(), Sort.BY_CREATED).isEmpty());
        //    Assert.assertFalse(taskService.findAllByProjectId(USER1.getId(), USER_TASK.getProjectId()).isEmpty());
        Assert.assertNotNull(taskService.findOneById(USER1.getId(), USER_TASK.getId()));
        Assert.assertNotNull(taskService.findOneByIndex(USER1.getId(), 0));
    }

    @Test
    public void remove() {
        Assert.assertNotNull(taskService.remove(USER1.getId(), USER_TASK));
        Assert.assertNotNull(taskService.remove(ADMIN_TASK));
    }

    @Test
    public void removeAll() {
        taskService.removeAll(USER1.getId());
        Assert.assertTrue(taskService.findAll(USER1.getId()).isEmpty());
    }

    @Test
    public void removeById() {
        Assert.assertNotNull(taskService.removeById(USER1.getId(), USER_TASK.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertNotNull(taskService.removeByIndex(USER1.getId(), 0));
    }

    @Test
    public void updateById() {
        Assert.assertNotNull(taskService.updateById(USER1.getId(), USER_TASK.getId(), "NEW NAME", "NEW DESCRIPTION"));
    }

    @Test
    public void updateByIndex() {
        Assert.assertNotNull(taskService.updateByIndex(USER1.getId(), 0, "NEW NAME", "NEW DESCRIPTION"));
    }

}
