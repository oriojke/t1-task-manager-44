package ru.t1.didyk.taskmanager.exception.entity;

public final class UserNotFoundException extends AbstractEntityNotFoundException {

    public UserNotFoundException() {
        super("Error! User not found.");
    }

}
