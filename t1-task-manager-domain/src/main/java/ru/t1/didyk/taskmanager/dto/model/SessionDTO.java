package ru.t1.didyk.taskmanager.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "sessions")
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column(name = "created")
    private Date date = new Date();

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = null;

}
