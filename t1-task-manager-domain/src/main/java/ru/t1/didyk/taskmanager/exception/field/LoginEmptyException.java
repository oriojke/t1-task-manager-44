package ru.t1.didyk.taskmanager.exception.field;

public final class LoginEmptyException extends AbstractFieldException {

    public LoginEmptyException() {
        super("Error! Login is empty.");
    }

}
