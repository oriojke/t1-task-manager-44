package ru.t1.didyk.taskmanager.api;

import ru.t1.didyk.taskmanager.dto.request.AbstractRequest;
import ru.t1.didyk.taskmanager.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
