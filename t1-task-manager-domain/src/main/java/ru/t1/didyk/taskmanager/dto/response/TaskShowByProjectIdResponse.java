package ru.t1.didyk.taskmanager.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowByProjectIdResponse extends AbstractResponse {

    @Nullable
    List<TaskDTO> tasks;

    public TaskShowByProjectIdResponse(@Nullable List<TaskDTO> tasks) {
        this.tasks = tasks;
    }
}
