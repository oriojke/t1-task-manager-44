package ru.t1.didyk.taskmanager.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.model.IWBS;
import ru.t1.didyk.taskmanager.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "projects")
public class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private final static long serialVersionUID = 1;

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    private Date created = new Date();

    public ProjectDTO(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }
}
